/*
 * @file HTML Buttons plugin for CKEditor
 * Copyright (C) 2012 Alfonso Martínez de Lizarrondo
 * A simple plugin to help create custom buttons to insert HTML blocks
 */

CKEDITOR.plugins.add('custombuttons',
{
    init: function (editor) {
        var buttonsConfig = editor.config.custombuttons;
        if (!buttonsConfig)
            return;

        function createCommand(definition) {
            return {
                exec: function (editor) {
                    editor.insertHtml(definition.html);
                }
            };
        }

        // Create the command for each button
        for (var i = 0; i < buttonsConfig.length; i++) {
            var button = buttonsConfig[i];
            var commandName = button.name;
            editor.addCommand(commandName, createCommand(button, editor));

            editor.ui.addButton(commandName,
			{
			    label: button.title,
			    command: commandName,
			    icon: this.path + button.icon
			});
        }
    } //Init

});

/**
 * An array of buttons to add to the toolbar.
 * Each button is an object with these properties:
 *	name: The name of the command and the button (the one to use in the toolbar configuration)
 *	icon: The icon to use. Place them in the plugin folder
 *	html: The HTML to insert when the user clicks the button
 *	title: Title that appears while hovering the button
 *
 * Default configuration with some sample buttons:
 */
CKEDITOR.config.custombuttons = [
	{
	    name: 'button1',
	    icon: 'icon1.png',
	    html: '<section class="margin-t10"><div class="container"><div class="row"><div class="col-md-12 margin-b30"><h3>SİYAH BAŞLIK <b class="color2">KIRMIZI BAŞLIK</b></h3></div><div class="col-md-6"><p class="margin-b15">METİN 1</p></div><div class="col-md-6"><p class="margin-b15">METİN 2</p></div></div></div></section>',
	    title: 'Başlık ve ikili içerik alanı.'
	},
	{
	    name: 'button2',
	    icon: 'icon2.png',
	    html: '<section class="margin-t50"><div class="container-fluid"><div class="row disp-flex"><div class=" col-md-6 col-1"><div class="row"><div class="col-md-9 ch-md-offset-2-1 ch-lg-offset-2-1"><h3>Başlık</h3><p>İçerik Alanı İçerik Alanı İçerik Alanı İçerik Alanı</p></div></div></div><div class="col-md-6 col-2"><div class="row"><div class="col-md-9 ch-md-offset-1-1"><h3 class="color1">Başlık</h3><p class="color1">İçerik Alanı İçerik Alanı İçerik Alanı İçerik Alanı</p></div></div></div></div></div></section>',
	    title: 'Kırmızı ve gri arkaplanlı, ikili içerik alanı.'
	},
	{
	    name: 'button3',
	    icon: 'icon3.png',
	    html: '<section class="margin-t50"><div class="container-fluid"><div class="row" style="background-color:#414141"><div class="container"><div class="row"><div class="col-md-12 margin-t15 margin-b30"><h3 class="color1">BAŞLIK</h3><p class="color1">İÇERİK</p></div></div></div></div></div></section>',
	    title: 'Gri arkaplanlı, başlıklı, tam sayfa içerik alanı.'
	},
	{
	    name: 'button4',
	    icon: 'icon4.png',
	    html: '<div><section class="margin-t50"><div class="container-fluid" style="background-color:#e33b31;color:#fff;"><div class="container"><div class="row row-eq-height"><div class="col-md-12 margin-t50 margin-b50"><h3>BAŞLIK</h3><p>İÇERİK</p></div></div></div></div></section></div>',
	    title: 'Kırmızı arkaplanlı, başlıklı, tam sayfa içerik alanı.'
	},
	{
	    name: 'button5',
	    icon: 'icon5.png',
	    html: '<div><section class="margin-t25"><div class="carousel slide"><div class="embed-responsive embed-responsive-16by9"><div class="video-grain" data-toggle="modal" data-target="#myModal"><video width="100%" height="200" autoplay="autoplay" muted><source src="!embedVideo!" type="video/webm; codecs=vp8,vorbis" style="height:200px"></video><span class="play-btn"></span></div></div></div></section></div>',
	    title: 'Tam sayfa video alanı.'
	},
    //{
    //    name: 'button6',
    //    icon: 'icon6.png',
    //    html: '<div  class="col-md margin-t30 margin-b50"><ul class="nav nav-tabs" role="tablist"><li role="presentation" class="active"><a href="#tb1" aria-controls="tb1" role="tab" data-toggle="tab">.istanbul acretion kayıt ope</a></li><li role="presentation"><a href="#tb2" aria-controls="tb2" role="tab" data-toggle="tab">.ist acretion kayıt ope</a></li></ul><div class="tab-content"><div role="tabpanel" class="tab-pane active margin-b20" id="tb1"><div class="col-md-3"><img src="assets/img/mlogo_1.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p> </div><div class="col-md-3"><img src="assets/img/mlogo_2.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div><div class="col-md-3"><img src="assets/img/mlogo_2.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div><div class="col-md-3"><img src="assets/img/mlogo_1.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div></div><div role="tabpanel" class="tab-pane margin-b20" id="tb2"><div class="col-md-3"><img src="assets/img/mlogo_2.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div><div class="col-md-3"><img src="assets/img/mlogo_1.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div><div class="col-md-3"><img src="assets/img/mlogo_2.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div><div class="col-md-3"><img src="assets/img/mlogo_1.jpg" class="img-responsive" /><p class="text-center">Yazı Alanı</p></div></div></div></div>',
    //    title: 'Table Acredition.'
    //},
    {
        name: 'button6',
        icon: 'icon6.png',
        html: '<section class="margin-t25"><div class="container"><div class="row"><div class="col-md-12"><h3 class="title margin-b20">Koyu Başlık<span>Kırmızı Başlık </span> </h3></div><div class="col-md-12 margin-b30"><p>İçerik Yazısı Buraya  İçerik Yazısı Buraya İçerik Yazısı Buraya İçerik Yazısı Buraya İçerik Yazısı Buraya İçerik Yazısı Buraya </p></div> </div></div></section>',
        title: 'Düz İçerik Sayfası.'
    },
];