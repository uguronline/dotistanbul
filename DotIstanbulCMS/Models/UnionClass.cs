﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace consulta.Models
{
    public class CareerList
    {
        public int careerId { get; set; }
        public string CareerCv { get; set; }
        public string CareerPath { get; set; }
        public string careerType { get; set; }
        public string Part { get; set; }
        public string Content { get; set; }
        public DateTime? AddDate { get; set; }
    }
}