﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace consulta.Models
{
    public static class ExtensionMethod
    {
        public static int ToInt32(this string s)
        {
            if(s == null || s == "")
                return 0;
            else
                return Int32.Parse(s);
        }

        public static string ConvertString(this int i)
        {
            return i.ToString();
        }

        public static bool ConvertToBoolean(this string s)
        {
            if(s == "false")
                return false;
            else if(s == "true")
                return true;
            else
                return false;
        }

        public static DateTime ConvertDateTime(this string s)
        {
            if(s != null)
                return Convert.ToDateTime(s);
            else
                return Convert.ToDateTime("00.00.0000 00:00:00");
        }

        public static int ObjectConvertToInt(this object s)
        {
            if(s == null || s == "")
                return 0;
            return Convert.ToInt32(s);
        }

        public static string IllegalCharRemove(this object s)
        {
            var x = s;
            if(s == null)
                return "";
            x = x.ToString().Replace('-',' ');
            x = x.ToString().Replace('!',' ');
            x = x.ToString().Replace('^',' ');
            x = x.ToString().Replace('+',' ');
            x = x.ToString().Replace('%',' ');
            x = x.ToString().Replace('&',' ');
            x = x.ToString().Replace('/',' ');
            x = x.ToString().Replace('(',' ');
            x = x.ToString().Replace(')',' ');
            x = x.ToString().Replace('=',' ');
            x = x.ToString().Replace('*',' ');
            x = x.ToString().Replace(',',' ');
            x = x.ToString().Replace('.',' ');
            x = x.ToString().Replace('ç',' ');
            x = x.ToString().Replace('"',' ');
            x = x.ToString().Replace('~',' ');
            x = x.ToString().Replace("'","");
            x = x.ToString().Replace("q","");
            x = x.ToString().Replace("w","");
            x = x.ToString().Replace("e","");
            x = x.ToString().Replace("r","");
            x = x.ToString().Replace("t","");
            x = x.ToString().Replace("y","");
            x = x.ToString().Replace("u","");
            x = x.ToString().Replace("ı","");
            x = x.ToString().Replace("o","");
            x = x.ToString().Replace("p","");
            x = x.ToString().Replace("ğ","");
            x = x.ToString().Replace("ü","");
            x = x.ToString().Replace("a","");
            x = x.ToString().Replace("s","");
            x = x.ToString().Replace("d","");
            x = x.ToString().Replace("f","");
            x = x.ToString().Replace("g","");
            x = x.ToString().Replace("h","");
            x = x.ToString().Replace("j","");
            x = x.ToString().Replace("k","");
            x = x.ToString().Replace("l","");
            x = x.ToString().Replace("ş","");
            x = x.ToString().Replace("i","");
            x = x.ToString().Replace(",","");
            x = x.ToString().Replace("<","");
            x = x.ToString().Replace(">","");
            x = x.ToString().Replace("z","");
            x = x.ToString().Replace("x","");
            x = x.ToString().Replace("c","");
            x = x.ToString().Replace("v","");
            x = x.ToString().Replace("b","");
            x = x.ToString().Replace("n","");
            x = x.ToString().Replace("m","");
            x = x.ToString().Replace("ö","");
            x = x.ToString().Replace("ç","");
            x = x.ToString().Replace(".","");


            return x.ToString();
        }
        public static bool EnumToBoolean(this int x)
        {
            if(x == 0)
                return false;
            return true;
        }
    }
    public class MyClass
    {
        public DataTable ListConvertToDataTable<T>(List<T> obj) where T:class
        {
            DataTable dt = new DataTable();

            var properties = obj.FirstOrDefault();

            Type t = properties.GetType();

            var columns = t.GetProperties().Select(a => a.Name).ToList();

            foreach(var item in columns)
            {
                dt.Columns.Add(item);
            }

            foreach(var item in obj)
            {
                var type = item.GetType();
                for(int k = 0;k < columns.Count;k++)
                {
                    dt.Rows.Add(type.GetProperty(columns[k]).GetValue(item));
                }
            }

            return dt;
           
            //var attachment = "attachment; filename=city.xls";
            //HttpContext.Current.Response.ClearContent();
            //HttpContext.Current.Response.AddHeader("content-disposition",attachment);
            //HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            //var tab = "";
            //foreach(DataColumn dc in dt.Columns)
            //{
            //    HttpContext.Current.Response.Write(tab + dc.ColumnName);
            //    tab = "\t";
            //}
            //HttpContext.Current.Response.Write("\n");
            //int i;
            //foreach(DataRow dr in dt.Rows)
            //{
            //    tab = "";
            //    for(i = 0;i < dt.Columns.Count;i++)
            //    {
            //        HttpContext.Current.Response.Write(tab + dr[ i ].ToString());
            //        tab = "\t";
            //    }
            //    HttpContext.Current.Response.Write("\n");
            //}
            //HttpContext.Current.Response.End();
        }
    }
}