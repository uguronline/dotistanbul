﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DotIstanbulCMS.Startup))]
namespace DotIstanbulCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
