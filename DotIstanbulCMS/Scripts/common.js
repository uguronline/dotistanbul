﻿$(document).ready(function () {
    $('#ChangeLanguage').change(function () {

        $.ajax({
            url: '/Admin/AdminLanguage',
            type: 'POST',
            dataType: 'Json',
            data: { language: $(this).val() },
            success: function (data) {
            },
            error: function (err) {
            }
        });
        location.reload();
    });

    $('#login').click(function () {
        var email = $('#req1').val();
        var password = $('#req2').val();
        if (email != "" && password != "") {

            $.ajax({
                type: 'POST',
                url: '/Admin/Login',
                dataType: 'Json',
                data: { email: email, password: password },
                success: function (data) {
                    if (data == true)
                        window.location.href = "/Admin/Index";
                    else
                        alert("Kullanıcı adı veya şifre yanlış!");
                },
                error: function (err) {
                    alert(err);
                }
            });

        }
    });

    $('#logout').click(function () {
        $.ajax({
            type: 'POST',
            url: '/Admin/LogOut',
            dataType: 'Json',
            success: function (data) {
                if (data == true)
                    window.location = "/Admin/Login";
            },
            error: function (err) {
            }
        });
    });

    //PanelUsers Start
    $('.panelusersIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelUsersIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //PanelUser End

    //PanelMenu Start
    $('.panelmenuIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelMenuIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PanelMenuSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PanelMenuSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //PanelMenu End

    //SocialMedia Start
    $('.socialmediaIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SocialMediaIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SocialMediaSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SocialMediaSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //SocialMedia End

    //Languages Start
    $('.languagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/LanguagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#LanguagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/LanguagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Languages End

    //Pages Start
    $('.pagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Pages End

    //Slider Start
    $('.sliderIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SliderIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SliderSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SliderSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Slider End

    //News Start
    $('.newsIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/NewsIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //News End

    //FaqCategories Start
    $('.faqcategoriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/FaqCategoriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#FaqCategoriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/FaqCategoriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //FaqCategories End

    //Faq Start
    $('.faqIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/FaqIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#FaqSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/FaqSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Faq End

    //Policies Start
    $('.policiesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PoliciesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PoliciesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PoliciesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Policies End

    //RegistrarCompanies Start
    $('.registrarcompaniesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/RegistrarCompaniesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#RegistrarCompaniesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/RegistrarCompaniesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //RegistrarCompanies End
});