﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using DAL;
using DAL.Repo;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;
using System.Net;

namespace DotIstanbulCMS.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        public void CreateSession()
        {
            if (Session["AdminLanguage"] == null)
            {
                Session["AdminLanguage"] = 1;
            }
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult LeftMenu()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return PartialView(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            var result = AdminMainProgress.AdminLogin(email, password, db);

            if (result != null)
            {
                Session.Add("LoginId", result.Id);
                Session.Add("Name", result.Name);
                Session.Add("Surname", result.Surname);
                Session.Add("Login", result.Role);
                Session.Add("AdminLanguage", 1);
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session["LoginId"] = null;
            Session["Name"] = null;
            Session["Surname"] = null;
            Session["Login"] = null;
            return Json(true);
        }

        [ChildActionOnly]
        public ActionResult ChangeLanguage()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return PartialView(main);
        }

        [HttpPost]
        public ActionResult AdminLanguage(string language)
        {
            Session["AdminLanguage"] = language;
            return Json(language);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //PanelUsers Start
        public ActionResult PanelUsers()
        {
            CreateSession();
            var result = AdminMainProgress.PanelUsersDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelUsers");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelUsers = main.GetPanelUsers();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersSave(FormCollection fc)
        {
            AdminMainProgress.PanelUsersSave(fc["Email"], fc["Password"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelUsersUpdate(fc["Id"], fc["Email"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        public ActionResult PanelUsersIsActive(string Id, string Active)
        {
            var result = AdminMainProgress.PanelUsersIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //PanelUsers End

        //PanelMenu Start
        public ActionResult PanelMenu()
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelMenu");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuSave(FormCollection fc)
        {
            AdminMainProgress.PanelMenuSave(int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelMenuUpdate(fc["Id"], int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        public ActionResult PanelMenuIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PanelMenuSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PanelMenuSort(ids, Session["Login"], db);
            return Json("success");
        }
        //PanelMenu End

        //Preferences Start
        public ActionResult Preferences()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Preferences = main.GetPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.PreferencesUpdate(fc["Logo"], fc["Favicon"], fc["MainImage"], fc["GoogleAnalytics"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Preferences");
        }
        //Preferences End

        //Languages Start
        public ActionResult Languages()
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Languages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesSave(FormCollection fc)
        {
            AdminMainProgress.LanguagesSave(fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesUpdate(FormCollection fc)
        {
            AdminMainProgress.LanguagesUpdate(fc["Id"], fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        public ActionResult LanguagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult LanguagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.LanguagesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //Languages End

        //SocialMedia Start
        public ActionResult SocialMedia()
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("SocialMedia");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSocialMedia = main.GetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaSave(FormCollection fc)
        {
            AdminMainProgress.SocialMediaSave(fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaUpdate(FormCollection fc)
        {
            AdminMainProgress.SocialMediaUpdate(fc["Id"], fc["Name"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        public ActionResult SocialMediaIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SocialMediaSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SocialMediaSort(ids, Session["Login"], db);
            return Json("success");
        }
        //SocialMedia End

        //MultiPreferences Start
        public ActionResult MultiPreferences()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllMultiPreferences = main.GetMultiPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MultiPreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.MultiPreferencesUpdate(int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Description"], fc["Keywords"], fc["Footer"], fc["Phone"], fc["Fax"], fc["Email"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("MultiPreferences");
        }
        //MultiPreferences End

        //Pages Start
        public ActionResult Pages()
        {
            CreateSession();
            var result = AdminMainProgress.PagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Pages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesSave(FormCollection fc)
        {
            AdminMainProgress.PagesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Content"], fc["Banner"], fc["UploadedVideo"], fc["EmbedVideo"], fc["View"], int.Parse(fc["Form"].ToString()), fc["OuterLink"], fc["ShowMenu"], fc["ShowFooter"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesUpdate(FormCollection fc)
        {
            AdminMainProgress.PagesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Content"], fc["Banner"], fc["UploadedVideo"], fc["EmbedVideo"], fc["View"], int.Parse(fc["Form"].ToString()), fc["OuterLink"], fc["ShowMenu"], fc["ShowFooter"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        public ActionResult PagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PagesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Pages End

        //Translates Start
        public ActionResult Translates()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllTranslates = main.GetTranslates();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesSave(FormCollection fc)
        {
            AdminMainProgress.TranslatesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesUpdate(FormCollection fc)
        {
            AdminMainProgress.TranslatesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }
        //Translates End

        //Slider Start
        public ActionResult Slider()
        {
            CreateSession();
            var result = AdminMainProgress.SliderDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Slider");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSliders = main.GetSlider();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderSave(FormCollection fc)
        {
            AdminMainProgress.SliderSave(int.Parse(Session["AdminLanguage"].ToString()), fc["Text"], fc["ShortText"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderUpdate(FormCollection fc)
        {
            AdminMainProgress.SliderUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), fc["Text"], fc["ShortText"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        public ActionResult SliderIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SliderIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SliderSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SliderSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Slider End

        //News Start
        public ActionResult News()
        {
            CreateSession();
            var result = AdminMainProgress.NewsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("News");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllNews = main.GetNews();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewsSave(FormCollection fc)
        {
            AdminMainProgress.NewsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Image"], Convert.ToDateTime(fc["Date"]), fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("News");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewsUpdate(FormCollection fc)
        {
            AdminMainProgress.NewsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Image"], Convert.ToDateTime(fc["Date"]), fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("News");
        }

        [HttpPost]
        public ActionResult NewsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.NewsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //News End

        //MainPageItems Start
        public ActionResult MainPageItems()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.MainPageItems = main.GetMainPageItems(int.Parse(Session["AdminLanguage"].ToString()));
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MainPageItemsUpdate(FormCollection fc)
        {
            AdminMainProgress.MainPageItemsUpdate(int.Parse(Session["AdminLanguage"].ToString()), fc["TopImage"], fc["TopContent"], fc["UploadedVideo"], fc["EmbedVideo"], fc["BottomImage"], fc["BottomContent"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("MainPageItems");
        }
        //MainPageItems End

        //FaqCategories Start
        public ActionResult FaqCategories()
        {
            CreateSession();
            var result = AdminMainProgress.FaqCategoriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("FaqCategories");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllFaqCategories = main.GetFaqCategories();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FaqCategoriesSave(FormCollection fc)
        {
            AdminMainProgress.FaqCategoriesSave(int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("FaqCategories");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FaqCategoriesUpdate(FormCollection fc)
        {
            AdminMainProgress.FaqCategoriesUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("FaqCategories");
        }

        [HttpPost]
        public ActionResult FaqCategoriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.FaqCategoriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult FaqCategoriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.FaqCategoriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //FaqCategories End

        //Faq Start
        public ActionResult Faq()
        {
            CreateSession();
            var result = AdminMainProgress.FaqDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Faq");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllFaqCategories = main.GetFaqCategories();
            main.AllFaq = main.GetFaq();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FaqSave(FormCollection fc)
        {
            AdminMainProgress.FaqSave(int.Parse(Session["AdminLanguage"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Question"], fc["Answer"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Faq");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult FaqUpdate(FormCollection fc)
        {
            AdminMainProgress.FaqUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), int.Parse(fc["ParentId"].ToString()), fc["Question"], fc["Answer"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Faq");
        }

        [HttpPost]
        public ActionResult FaqIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.FaqIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult FaqSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.FaqSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Faq End

        //Policies Start
        public ActionResult Policies()
        {
            CreateSession();
            var result = AdminMainProgress.PoliciesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Policies");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPolicies = main.GetPolicies();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PoliciesSave(FormCollection fc)
        {
            AdminMainProgress.PoliciesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Content"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Policies");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PoliciesUpdate(FormCollection fc)
        {
            AdminMainProgress.PoliciesUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Content"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Policies");
        }

        [HttpPost]
        public ActionResult PoliciesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PoliciesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PoliciesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PoliciesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Policies End

        //Newsletter Start
        public ActionResult Newsletter()
        {
            CreateSession();
            var result = AdminMainProgress.NewsletterDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Newsletter");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Newsletter = main.GetNewsletter();
            return View(main);
        }
        //Newsletter End

        //ApplicationForm Start
        public ActionResult ApplicationForm()
        {
            CreateSession();
            var result = AdminMainProgress.ApplicationFormDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ApplicationForm");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.ApplicationForm = main.GetApplicationForm();
            return View(main);
        }
        //ApplicationForm End

        //ContactForm Start
        public ActionResult ContactForm()
        {
            CreateSession();
            var result = AdminMainProgress.ContactFormDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ContactForm");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.ContactForm = main.GetContactForm();
            return View(main);
        }
        //ContactForm End

        //RegistrarCompanies Start
        public ActionResult RegistrarCompanies()
        {
            CreateSession();
            var result = AdminMainProgress.RegistrarCompaniesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("RegistrarCompanies");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllRegistrarCompanies = main.GetRegistrarCompanies();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult RegistrarCompaniesSave(FormCollection fc)
        {
            AdminMainProgress.RegistrarCompaniesSave(fc["Name"], fc["Logo"], fc["Country"], fc["Ist"], fc["Istanbul"], fc["Url"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("RegistrarCompanies");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult RegistrarCompaniesUpdate(FormCollection fc)
        {
            AdminMainProgress.RegistrarCompaniesUpdate(fc["Id"], fc["Name"], fc["Logo"], fc["Country"], fc["Ist"], fc["Istanbul"], fc["Url"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("RegistrarCompanies");
        }

        [HttpPost]
        public ActionResult RegistrarCompaniesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.RegistrarCompaniesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult RegistrarCompaniesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.RegistrarCompaniesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //RegistrarCompanies End
    }
}