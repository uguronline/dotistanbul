﻿using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using System.Configuration;
namespace DAL
{
    public class AdminMainProgress
    {
        private int _login;

        public List<PanelMenu> AllPanelMenu { get; set; }
        public List<PanelUsers> AllPanelUsers { get; set; }
        public List<Preferences> Preferences { get; set; }
        public List<SocialMedia> AllSocialMedia { get; set; }
        public List<Languages> AllLanguages { get; set; }
        public Languages Languages { get; set; }
        public List<MultiPreferences> AllMultiPreferences { get; set; }
        public MultiPreferences MultiPreferences { get; set; }
        public List<Pages> AllPages { get; set; }
        public Pages Pages { get; set; }
        public List<Translates> AllTranslates { get; set; }
        public List<Slider> AllSliders { get; set; }
        public List<News> AllNews { get; set; }
        public News News { get; set; }
        public List<Faq> AllFaq { get; set; }
        public List<FaqCategories> AllFaqCategories { get; set; }
        public List<MainPageItems> MainPageItems { get; set; }
        public MainPageItems MainPageItem { get; set; }
        public List<Newsletter> Newsletter { get; set; }
        public List<ApplicationForm> ApplicationForm { get; set; }
        public List<ContactForm> ContactForm { get; set; }
        public List<Policies> AllPolicies { get; set; }
        public Policies Policies { get; set; }
        public RegistrarCompanies RegistrarCompanies { get; set; }
        public List<RegistrarCompanies> AllRegistrarCompanies { get; set; }

        DatabaseConnection db = new DatabaseConnection();

        public AdminMainProgress(object login)
        {
            this._login = login.ObjectConvertToInt();
        }

        public static PanelUsers AdminLogin(string email, string password, DatabaseConnection db)
        {
            string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            var user = db.PanelUsers.Where(x => x.Email == email && x.Password == hashedPassword && x.Active == true && x.Deleted == false).FirstOrDefault();
            return user;
        }

        //PanelUser Start
        public List<PanelUsers> GetPanelUsers()
        {
            var list = db.PanelUsers.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelUsersSave(string Email, string Password, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false).FirstOrDefault();

            if (isHave == null)
            {
                string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = new PanelUsers();
                paneluser.Email = Email;
                paneluser.Password = hashedPassword;
                paneluser.Name = Name;
                paneluser.Surname = Surname;
                paneluser.Role = Role;
                paneluser.Owner = Owner;
                paneluser.CreatedDate = DateTime.Now;
                paneluser.Active = DataActive;
                paneluser.Deleted = false;
                db.PanelUsers.Add(paneluser);
                db.SaveChanges();
            }
        }

        public static void PanelUsersUpdate(string Id, string Email, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false && x.Id != ConvertId).FirstOrDefault();

            if (isHave == null)
            {
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
                if (paneluser != null)
                {
                    paneluser.Email = Email;
                    paneluser.Name = Name;
                    paneluser.Surname = Surname;
                    paneluser.Role = Role;
                    paneluser.Owner = Owner;
                    paneluser.UpdatedDate = DateTime.Now;
                    paneluser.Active = DataActive;
                    db.SaveChanges();
                }
            }
        }

        public static string PanelUsersDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelUsers.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelUsersIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var paneluserUpdate = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            paneluserUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //PanelUser End

        //PanelMenu Start
        public List<PanelMenu> GetPanelMenu()
        {
            var list = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelMenuSave(int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = new PanelMenu();
            panelmenu.ParentId = ParentId;
            panelmenu.Name = Name;
            panelmenu.Url = Url;
            panelmenu.Redirect = Redirect;
            panelmenu.Icon = "dash";
            panelmenu.Access = Access;
            panelmenu.Order = db.PanelMenu.Count() + 1;
            panelmenu.Active = DataActive;
            panelmenu.Deleted = false;
            db.PanelMenu.Add(panelmenu);
            db.SaveChanges();
        }

        public static void PanelMenuUpdate(string Id, int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (panelmenu != null)
            {
                panelmenu.ParentId = ParentId;
                panelmenu.Name = Name;
                panelmenu.Url = Url;
                panelmenu.Redirect = Redirect;
                panelmenu.Icon = "dash";
                panelmenu.Access = Access;
                panelmenu.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string PanelMenuDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelMenu.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelMenuIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var panelmenuUpdate = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            panelmenuUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PanelMenuSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //PanelMenu End

        //Preferences Start
        public List<Preferences> GetPreferences()
        {
            var list = db.Preferences.Where(x => x.Id == 1).ToList();
            return list;
        }

        public static void PreferencesUpdate(string Logo, string Favicon, string MainImage, string GoogleAnalytics, int Owner, object login, DatabaseConnection db)
        {
            var preferences = db.Preferences.Where(x => x.Id == 1).FirstOrDefault();
            if (preferences != null)
            {
                preferences.Logo = Logo;
                preferences.Favicon = Favicon;
                preferences.MainImage = MainImage;
                preferences.GoogleAnalytics = GoogleAnalytics;
                preferences.UpdatedDate = DateTime.Now;
                preferences.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Preferences End

        //SocialMedia Start
        public List<SocialMedia> GetSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SocialMediaSave(string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = new SocialMedia();
            socialmedia.Name = Name;
            socialmedia.Link = Link;
            socialmedia.Order = db.SocialMedia.Count() + 1;
            socialmedia.CreatedDate = DateTime.Now;
            socialmedia.Owner = Owner;
            socialmedia.Active = DataActive;
            socialmedia.Deleted = false;
            db.SocialMedia.Add(socialmedia);
            db.SaveChanges();
        }

        public static void SocialMediaUpdate(string Id, string Name, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (socialmedia != null)
            {
                socialmedia.Name = Name;
                socialmedia.Link = Link;
                socialmedia.UpdatedDate = DateTime.Now;
                socialmedia.Owner = Owner;
                socialmedia.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SocialMediaDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.SocialMedia.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SocialMediaIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var socialmediaUpdate = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            socialmediaUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SocialMediaSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //SocialMedia End

        //Languages Start
        public List<Languages> GetLanguages()
        {
            var list = db.Languages.Where(x => x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public static void LanguagesSave(string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = new Languages();
            languages.Name = Name;
            languages.ShortName = ShortName;
            languages.Order = db.Languages.Count() + 1;
            languages.CreatedDate = DateTime.Now;
            languages.Owner = Owner;
            languages.Active = DataActive;
            languages.Deleted = false;
            db.Languages.Add(languages);
            db.SaveChanges();
        }

        public static void LanguagesUpdate(string Id, string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (languages != null)
            {
                languages.Name = Name;
                languages.ShortName = ShortName;
                languages.UpdatedDate = DateTime.Now;
                languages.Owner = Owner;
                languages.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string LanguagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Languages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool LanguagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var languagesUpdate = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            languagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool LanguagesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Languages.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Languages End

        public object SelectLanguage(object LanguageId)
        {
            if (LanguageId == null)
            {
                LanguageId = 1;
            }

            return LanguageId;
        }

        //MultiPreferences Start
        public List<MultiPreferences> GetMultiPreferences()
        {
            var list = db.MultiPreferences.ToList();
            return list;
        }

        public static void MultiPreferencesUpdate(int LanguageId, string Title, string Description, string Keywords, string Footer, string Phone, string Fax, string Email, int Owner, object login, DatabaseConnection db)
        {
            var multipreferences = db.MultiPreferences.Where(x => x.LanguageId == LanguageId).FirstOrDefault();
            if (multipreferences != null)
            {
                multipreferences.LanguageId = LanguageId;
                multipreferences.Title = Title;
                multipreferences.Description = Description;
                multipreferences.Keywords = Keywords;
                multipreferences.Footer = Footer;
                multipreferences.Phone = Phone;
                multipreferences.Fax = Fax;
                multipreferences.Email = Email;
                multipreferences.UpdatedDate = DateTime.Now;
                multipreferences.Owner = Owner;
                db.SaveChanges();
            }
            else
            {
                var multipreferences2 = new MultiPreferences();
                multipreferences2.LanguageId = LanguageId;
                multipreferences2.Title = Title;
                multipreferences2.Description = Description;
                multipreferences2.Keywords = Keywords;
                multipreferences2.Footer = Footer;
                multipreferences2.Phone = Phone;
                multipreferences2.Fax = Fax;
                multipreferences2.Email = Email;
                multipreferences2.UpdatedDate = DateTime.Now;
                multipreferences2.Owner = Owner;
                db.MultiPreferences.Add(multipreferences2);
                db.SaveChanges();
            }
        }
        //MultiPreferences End

        //Pages Start
        public List<Pages> GetPages()
        {
            var list = db.Pages.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PagesSave(int DataId, int LanguageId, int ParentId, string Title, string Content, string Banner, string UploadedVideo, string EmbedVideo, string View, int Form, string OuterLink, string ShowMenu, string ShowFooter, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;

            var DataShowFooter = false;
            if (ShowFooter == "0")
                DataShowFooter = true;

            var pages = new Pages();
            if (DataId != 0) pages.DataId = DataId;
            pages.LanguageId = LanguageId;
            pages.ParentId = ParentId;
            pages.Title = Title;
            pages.Content = Content;
            pages.Banner = Banner;
            pages.UploadedVideo = UploadedVideo;
            pages.EmbedVideo = EmbedVideo;
            pages.View = View;
            pages.Form = Form;
            pages.OuterLink = OuterLink;
            pages.ShowMenu = DataShowMenu;
            pages.ShowFooter = DataShowFooter;
            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            pages.Keywords = Keywords;
            pages.Description = Description;
            pages.Order = db.Pages.Count() + 1;
            pages.CreatedDate = DateTime.Now;
            pages.Owner = Owner;
            pages.Active = DataActive;
            pages.Deleted = false;

            //Friendly Url
            var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

            if (FriendlyUrl == string.Empty)
            {
                if (ParentData != null)
                {
                    FriendlyUrl = ParentData.Title + " " + Title;
                }
                else
                {
                    FriendlyUrl = Title;
                }
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Pages.Add(pages);
            db.SaveChanges();

            if (DataId == 0)
            {
                var pages2 = db.Pages.Where(x => x.Id == pages.Id).FirstOrDefault();
                pages2.DataId = pages.Id;
                db.SaveChanges();
            }
        }

        public static void PagesUpdate(string Id, int DataId, int LanguageId, int ParentId, string Title, string Content, string Banner, string UploadedVideo, string EmbedVideo, string View, int Form, string OuterLink, string ShowMenu, string ShowFooter, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;
            var DataShowMenu = false;
            if (ShowMenu == "0")
                DataShowMenu = true;
            var DataShowFooter = false;
            if (ShowFooter == "0")
                DataShowFooter = true;

            var pages = db.Pages.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (pages != null)
            {
                pages.DataId = DataId;
                pages.ParentId = ParentId;
                pages.Title = Title;
                pages.Content = Content;
                pages.Banner = Banner;
                pages.UploadedVideo = UploadedVideo;
                pages.EmbedVideo = EmbedVideo;
                pages.View = View;
                pages.Form = Form;
                pages.OuterLink = OuterLink;
                pages.ShowMenu = DataShowMenu;
                pages.ShowFooter = DataShowFooter;
                pages.Keywords = Keywords;
                pages.Description = Description;
                pages.UpdatedDate = DateTime.Now;
                pages.Owner = Owner;
                pages.Active = DataActive;

                //Friendly Url
                var ParentData = db.Pages.Where(x => x.DataId == ParentId && x.LanguageId == LanguageId).FirstOrDefault();

                if (FriendlyUrl == string.Empty)
                {
                    if (ParentData != null)
                    {
                        FriendlyUrl = ParentData.Title + " " + Title;
                    }
                    else
                    {
                        FriendlyUrl = Title;
                    }
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Pages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var pagesUpdate = db.Pages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            pagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PagesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Pages.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Pages End

        //Translates Start
        public List<Translates> GetTranslates()
        {
            var list = db.Translates.ToList();
            return list;
        }

        public static void TranslatesSave(int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var translates = new Translates();
            if (DataId != 0) translates.DataId = DataId;
            translates.LanguageId = LanguageId;
            translates.Description = Description;
            translates.Text = Text;
            translates.CreatedDate = DateTime.Now;
            translates.Owner = Owner;
            db.Translates.Add(translates);
            db.SaveChanges();

            if (DataId == 0)
            {
                var translates2 = db.Translates.Where(x => x.Id == translates.Id).FirstOrDefault();
                translates2.DataId = translates.Id;
                db.SaveChanges();
            }
        }

        public static void TranslatesUpdate(string Id, int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var translates = db.Translates.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId).FirstOrDefault();
            if (translates != null)
            {
                translates.DataId = DataId;
                translates.Description = Description;
                translates.Text = Text;
                translates.UpdatedDate = DateTime.Now;
                translates.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Translates End

        //Slider Start
        public List<Slider> GetSlider()
        {
            var list = db.Slider.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SliderSave(int LanguageId, string Text, string ShortText, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = new Slider();
            slider.LanguageId = LanguageId;
            slider.Text = Text;
            slider.ShortText = ShortText;
            slider.Image = Image;
            slider.Order = db.Slider.Count() + 1;
            slider.CreatedDate = DateTime.Now;
            slider.Owner = Owner;
            slider.Active = DataActive;
            slider.Deleted = false;
            db.Slider.Add(slider);
            db.SaveChanges();
        }

        public static void SliderUpdate(string Id, int LanguageId, string Text, string ShortText, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = db.Slider.Where(x => x.Id == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (slider != null)
            {
                slider.Text = Text;
                slider.ShortText = ShortText;
                slider.Image = Image;
                slider.UpdatedDate = DateTime.Now;
                slider.Owner = Owner;
                slider.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SliderDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Slider.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SliderIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var sliderUpdate = db.Slider.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            sliderUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SliderSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Slider.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Slider End

        //News Start
        public List<News> GetNews()
        {
            var list = db.News.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void NewsSave(int DataId, int LanguageId, string Title, string ShortContent, string Content, string Image, DateTime Date, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var news = new News();
            if (DataId != 0) news.DataId = DataId;
            news.LanguageId = LanguageId;
            news.Title = Title;
            news.ShortContent = ShortContent;
            news.Content = Content;
            news.Image = Image;
            news.Date = Date;
            news.Keywords = Keywords;
            news.Description = Description;
            news.CreatedDate = DateTime.Now;
            news.Owner = Owner;
            news.Active = DataActive;
            news.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.News.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                news.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.News.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        news.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.News.Add(news);
            db.SaveChanges();

            if (DataId == 0)
            {
                var news2 = db.News.Where(x => x.Id == news.Id).FirstOrDefault();
                news2.DataId = news.Id;
                db.SaveChanges();
            }
        }

        public static void NewsUpdate(string Id, int DataId, int LanguageId, string Title, string ShortContent, string Content, string Image, DateTime Date, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var news = db.News.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (news != null)
            {
                news.DataId = DataId;
                news.Title = Title;
                news.ShortContent = ShortContent;
                news.Content = Content;
                news.Image = Image;
                news.Date = Date;
                news.Keywords = Keywords;
                news.Description = Description;
                news.UpdatedDate = DateTime.Now;
                news.Owner = Owner;
                news.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.News.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    news.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.News.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            news.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string NewsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.News.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool NewsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var newsUpdate = db.News.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            newsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //News End

        //MainPageItems Start
        public List<MainPageItems> GetMainPageItems(int LanguageId)
        {
            var list = db.MainPageItems.Where(x => x.LanguageId == LanguageId).ToList();
            return list;
        }

        public static void MainPageItemsUpdate(int LanguageId, string TopImage, string TopContent, string UploadedVideo, string EmbedVideo, string BottomImage, string BottomContent, int Owner, object login, DatabaseConnection db)
        {
            var mainpageitems = db.MainPageItems.Where(x => x.LanguageId == LanguageId).FirstOrDefault();
            if (mainpageitems != null)
            {
                mainpageitems.LanguageId = LanguageId;
                mainpageitems.TopImage = TopImage;
                mainpageitems.TopContent = TopContent;
                mainpageitems.UploadedVideo = UploadedVideo;
                mainpageitems.EmbedVideo = EmbedVideo;
                mainpageitems.BottomImage = BottomImage;
                mainpageitems.BottomContent = BottomContent;
                mainpageitems.UpdatedDate = DateTime.Now;
                mainpageitems.Owner = Owner;
                db.SaveChanges();
            }
            else
            {
                var mainpageitems2 = new MainPageItems();
                mainpageitems2.LanguageId = LanguageId;
                mainpageitems2.TopImage = TopImage;
                mainpageitems2.TopContent = TopContent;
                mainpageitems2.UploadedVideo = UploadedVideo;
                mainpageitems2.EmbedVideo = EmbedVideo;
                mainpageitems2.BottomImage = BottomImage;
                mainpageitems2.BottomContent = BottomContent;
                mainpageitems2.UpdatedDate = DateTime.Now;
                mainpageitems2.Owner = Owner;
                db.MainPageItems.Add(mainpageitems2);
                db.SaveChanges();
            }
        }
        //MainPageItems End

        //FaqCategories Start
        public List<FaqCategories> GetFaqCategories()
        {
            var list = db.FaqCategories.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void FaqCategoriesSave(int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var faqcategories = new FaqCategories();
            faqcategories.LanguageId = LanguageId;
            faqcategories.Title = Title;
            faqcategories.Order = db.FaqCategories.Count() + 1;
            faqcategories.CreatedDate = DateTime.Now;
            faqcategories.Owner = Owner;
            faqcategories.Active = DataActive;
            faqcategories.Deleted = false;
            db.FaqCategories.Add(faqcategories);
            db.SaveChanges();
        }

        public static void FaqCategoriesUpdate(string Id, int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var faqcategories = db.FaqCategories.Where(x => x.Id == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (faqcategories != null)
            {
                faqcategories.LanguageId = LanguageId;
                faqcategories.Title = Title;
                faqcategories.UpdatedDate = DateTime.Now;
                faqcategories.Owner = Owner;
                faqcategories.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string FaqCategoriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faqcategories = db.FaqCategories.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faqcategories != null)
                        faqcategories.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool FaqCategoriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var faqcategoriesUpdate = db.FaqCategories.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            faqcategoriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool FaqCategoriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.FaqCategories.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //FaqCategories End

        //Faq Start
        public List<Faq> GetFaq()
        {
            var list = db.Faq.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void FaqSave(int LanguageId, int ParentId, string Question, string Answer, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var faq = new Faq();
            faq.LanguageId = LanguageId;
            faq.ParentId = ParentId;
            faq.Question = Question;
            faq.Answer = Answer;
            faq.Order = db.Faq.Count() + 1;
            faq.CreatedDate = DateTime.Now;
            faq.Owner = Owner;
            faq.Active = DataActive;
            faq.Deleted = false;
            db.Faq.Add(faq);
            db.SaveChanges();
        }

        public static void FaqUpdate(string Id, int LanguageId, int ParentId, string Question, string Answer, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var faq = db.Faq.Where(x => x.Id == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (faq != null)
            {
                faq.LanguageId = LanguageId;
                faq.ParentId = ParentId;
                faq.Question = Question;
                faq.Answer = Answer;
                faq.UpdatedDate = DateTime.Now;
                faq.Owner = Owner;
                faq.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string FaqDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Faq.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool FaqIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var faqUpdate = db.Faq.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            faqUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool FaqSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Faq.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Faq End

        //Newsletter Start
        public List<Newsletter> GetNewsletter()
        {
            var list = db.Newsletter.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string NewsletterDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Newsletter.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //Newsletter End

        //ApplicationForm Start
        public List<ApplicationForm> GetApplicationForm()
        {
            var list = db.ApplicationForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string ApplicationFormDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ApplicationForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //ApplicationForm End

        //ContactForm Start
        public List<ContactForm> GetContactForm()
        {
            var list = db.ContactForm.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string ContactFormDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ContactForm.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //ContactForm End

        //Policies Start
        public List<Policies> GetPolicies()
        {
            var list = db.Policies.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PoliciesSave(int DataId, int LanguageId, string Title, string Content, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var policies = new Policies();
            if (DataId != 0) policies.DataId = DataId;
            policies.LanguageId = LanguageId;
            policies.Title = Title;
            policies.Content = Content;
            policies.Keywords = Keywords;
            policies.Description = Description;
            policies.Order = db.Policies.Count() + 1;
            policies.CreatedDate = DateTime.Now;
            policies.Owner = Owner;
            policies.Active = DataActive;
            policies.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Policies.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                policies.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Policies.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        policies.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Policies.Add(policies);
            db.SaveChanges();

            if (DataId == 0)
            {
                var policies2 = db.Policies.Where(x => x.Id == policies.Id).FirstOrDefault();
                policies2.DataId = policies.Id;
                db.SaveChanges();
            }
        }

        public static void PoliciesUpdate(string Id, int LanguageId, string Title, string Content, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var policies = db.Policies.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (policies != null)
            {
                policies.Title = Title;
                policies.Content = Content;
                policies.Keywords = Keywords;
                policies.Description = Description;
                policies.UpdatedDate = DateTime.Now;
                policies.Owner = Owner;
                policies.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Policies.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    policies.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Policies.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            policies.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PoliciesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Policies.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PoliciesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var policiesUpdate = db.Policies.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            policiesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PoliciesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var policy = db.Policies.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = policy.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Policies End

        //RegistrarCompanies Start
        public List<RegistrarCompanies> GetRegistrarCompanies()
        {
            var list = db.RegistrarCompanies.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void RegistrarCompaniesSave(string Name, string Logo, string Country, string Ist, string Istanbul, string Url, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataIst = false;
            if (Ist == "0")
                DataIst = true;

            var DataIstanbul = false;
            if (Istanbul == "0")
                DataIstanbul = true;

            var registrarcompanies = new RegistrarCompanies();
            registrarcompanies.Name = Name;
            registrarcompanies.Logo = Logo;
            registrarcompanies.Country = Country;
            registrarcompanies.Ist = DataIst;
            registrarcompanies.Istanbul = DataIstanbul;
            registrarcompanies.Url = Url;
            registrarcompanies.Order = db.RegistrarCompanies.Count() + 1;
            registrarcompanies.CreatedDate = DateTime.Now;
            registrarcompanies.Owner = Owner;
            registrarcompanies.Active = DataActive;
            registrarcompanies.Deleted = false;
            db.RegistrarCompanies.Add(registrarcompanies);
            db.SaveChanges();
        }

        public static void RegistrarCompaniesUpdate(string Id, string Name, string Logo, string Country, string Ist, string Istanbul, string Url, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataIst = false;
            if (Ist == "0")
                DataIst = true;

            var DataIstanbul = false;
            if (Istanbul == "0")
                DataIstanbul = true;

            var registrarcompanies = db.RegistrarCompanies.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (registrarcompanies != null)
            {
                registrarcompanies.Name = Name;
                registrarcompanies.Logo = Logo;
                registrarcompanies.Country = Country;
                registrarcompanies.Ist = DataIst;
                registrarcompanies.Istanbul = DataIstanbul;
                registrarcompanies.Url = Url;
                registrarcompanies.UpdatedDate = DateTime.Now;
                registrarcompanies.Owner = Owner;
                registrarcompanies.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string RegistrarCompaniesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.RegistrarCompanies.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool RegistrarCompaniesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var registrarcompaniesUpdate = db.RegistrarCompanies.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            registrarcompaniesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool RegistrarCompaniesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.RegistrarCompanies.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //RegistrarCompanies End

        /////////////// <- Panel | Site -> ///////////////

        public List<Pages> SiteGetAllPages(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Pages> SiteGetHeaderMenu(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.ShowMenu == true && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Pages> SiteGetFooterMenu(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.ShowFooter == true && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Pages SiteGetPage(string friendlyUrl)
        {
            var single = db.Pages.Where(x => x.FriendlyUrl == friendlyUrl && x.Active == true && x.Deleted == false).FirstOrDefault();
            return single;
        }

        public List<SocialMedia> SiteGetAllSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<News> SiteGetAllNews(int SiteLanguage)
        {
            var list = db.News.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderByDescending(x => x.Date).ToList();
            return list;
        }

        public News SiteGetNew(string friendlyUrl)
        {
            var single = db.News.Where(x => x.FriendlyUrl == friendlyUrl && x.Active == true && x.Deleted == false).FirstOrDefault();
            return single;
        }

        public List<FaqCategories> SiteGetAllFaqCategories(int SiteLanguage)
        {
            var list = db.FaqCategories.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Faq> SiteGetAllFaq(int SiteLanguage)
        {
            var list = db.Faq.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Slider> SiteGetAllSlider(int SiteLanguage)
        {
            var list = db.Slider.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public MainPageItems SiteGetMainPageItem(int SiteLanguage)
        {
            var single = db.MainPageItems.Where(x => x.LanguageId == SiteLanguage).FirstOrDefault();
            return single;
        }

        public List<Languages> SiteGetAllLanguages()
        {
            var list = db.Languages.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Languages SiteGetLanguage(string ShortName)
        {
            var single = db.Languages.Where(x => x.ShortName == ShortName).FirstOrDefault();
            return single;
        }

        public List<Translates> SiteGetAllTranslates(int SiteLanguage)
        {
            var list = db.Translates.Where(x => x.LanguageId == SiteLanguage).ToList();
            return list;
        }

        public MultiPreferences SiteGetMultiPreferences(int SiteLanguage)
        {
            var single = db.MultiPreferences.Where(x => x.LanguageId == SiteLanguage).FirstOrDefault();
            return single;
        }

        public List<RegistrarCompanies> SiteGetAllRegistrarCompanies()
        {
            var list = db.RegistrarCompanies.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Policies> SiteGetAllPolicies(int SiteLanguage)
        {
            var list = db.Policies.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Policies SiteGetPolicies(string friendlyUrl)
        {
            var single = db.Policies.Where(x => x.FriendlyUrl == friendlyUrl && x.Active == true && x.Deleted == false).FirstOrDefault();
            return single;
        }

        public static void NewsletterSave(string FirstName, string LastName, string Email, DatabaseConnection db)
        {
            var newsletter = new Newsletter();
            newsletter.FirstName = FirstName;
            newsletter.LastName = LastName;
            newsletter.Email = Email;
            newsletter.CreatedDate = DateTime.Now;
            newsletter.Deleted = false;
            db.Newsletter.Add(newsletter);
            db.SaveChanges();
        }

        public static void ApplicationFormSave(string NameSurname, string Email, string Company, string City, string Country, string RegistrarName, string Website, string Notes, DatabaseConnection db)
        {
            var applicationform = new ApplicationForm();
            applicationform.NameSurname = NameSurname;
            applicationform.Email = Email;
            applicationform.Company = Company;
            applicationform.City = City;
            applicationform.Country = Country;
            applicationform.RegistrarName = RegistrarName;
            applicationform.Website = Website;
            applicationform.Notes = Notes;
            applicationform.CreatedDate = DateTime.Now;
            applicationform.Deleted = false;
            db.ApplicationForm.Add(applicationform);
            db.SaveChanges();
        }

        public static void ContactFormSave(string Name, string Surname, string Email, string Phone, string Subject, string Message, DatabaseConnection db)
        {
            var contactform = new ContactForm();
            contactform.Name = Name;
            contactform.Surname = Surname;
            contactform.Email = Email;
            contactform.Phone = Phone;
            contactform.Subject = Subject;
            contactform.Message = Message;
            contactform.CreatedDate = DateTime.Now;
            contactform.Deleted = false;
            db.ContactForm.Add(contactform);
            db.SaveChanges();
        }
    }
}