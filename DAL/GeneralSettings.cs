﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
    public class StaticData
    {
        public const string ImageSite = "http://localhost:45680";
        public static string Site = HttpContext.Current.Request.Url.Host;
        public const string FromEmail = "destek@hipervision.com";
        public const string EmailPassword = "D3n3m3";
        public const string Host = "mail.hipervision.com";
        public const int Port = 587;
        public const string To = "ugur@zoniletisim.com";
    }

    public class Enums
    {
        public static string[] FormViews = { null, "_Newsletter", "_ApplicationForm", "_ContactForm", "_RegistrarCompanies" };
        public static Dictionary<string, string> PageTypes = new Dictionary<string, string>()
        {
            {"news","haberler/"}, {"policies","politikalar/"}, {"faq","sik-sorulan-sorular/"}, {"whoisquery","sorgu/"}, {"custompages","kayit/"}
        };
    }
}
