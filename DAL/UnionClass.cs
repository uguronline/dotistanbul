﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PostList
    {
        public int Id { get; set; }
        public Nullable<int> UserId { get; set; }
        public string Content { get; set; }
        public string Image { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
