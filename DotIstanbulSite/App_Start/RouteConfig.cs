﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DotIstanbulSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ico");

            routes.MapRoute(
            name: "WhoisQuery",
            url: "whoisquery",
            defaults: new { controller = "Home", action = "WhoisQuery" }
            );

            routes.MapRoute(
            name: "Pages",
            url: "{friendlyUrl}",
            defaults: new { controller = "Home", action = "Pages" }
            );

            routes.MapRoute(
            name: "CustomPages",
            url: "kayit/{friendlyUrl}",
            defaults: new { controller = "Home", action = "CustomPages", friendlyUrl = "kayit" }
            );

            routes.MapRoute(
            name: "News",
            url: "haberler/{friendlyUrl}/{subFriendlyUrl}",
            defaults: new { controller = "Home", action = "News", friendlyUrl = "haberler", subFriendlyUrl = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Policies",
            url: "politikalar/{friendlyUrl}/{subFriendlyUrl}",
            defaults: new { controller = "Home", action = "Policies", friendlyUrl = "politikalar", subFriendlyUrl = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Faq",
            url: "sik-sorulan-sorular/{friendlyUrl}",
            defaults: new { controller = "Home", action = "Faq", friendlyUrl = "sik-sorulan-sorular" }
            );

            routes.MapRoute(
            name: "ChangeLanguage",
            url: "ChangeLanguage/{shortName}",
            defaults: new { controller = "Home", action = "ChangeLanguage", shortName = UrlParameter.Optional }
            );

            routes.MapRoute(
            name: "Default",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}