﻿using DAL;
using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Activation;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DotIstanbulSite.Controllers
{
    public class LanguageActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //HttpContextBase context = filterContext.RequestContext.HttpContext;
            //string cookie = context.Request.Cookies["culture"] != null && context.Request.Cookies["culture"].Value != "" ? context.Request.Cookies["culture"].Value : null;
            //if (cookie != null)
            //{
            //    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cookie);
            //    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cookie);
            //}
            //else
            //{
            //    string[] userBrowserLanguage = HttpContext.Current.Request.UserLanguages;
            //    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(userBrowserLanguage[0]);
            //    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(userBrowserLanguage[0]);
            //}

            HttpContextBase context = filterContext.RequestContext.HttpContext;
            string cookie = context.Request.Cookies["culture"] != null && context.Request.Cookies["culture"].Value != "" ? context.Request.Cookies["culture"].Value : ConfigurationManager.AppSettings["DefaultLanguage"];
            if (cookie != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cookie);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cookie);
            }
        }
    }

    public class HomeController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        public void ChangeLanguage(string ShortName)
        {
            Response.Cookies.Remove("culture");
            Response.Cookies.Add(new HttpCookie("culture")
            {
                Name = "culture",
                Expires = DateTime.Now.AddDays(3),
                Path = "/",
                Value = ShortName
            });
            Response.Redirect("/");
        }

        public ActionResult _Header(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllPages = main.SiteGetHeaderMenu(lang);
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllLanguages = main.SiteGetAllLanguages();
            return View(main);
        }

        public ActionResult _Footer(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllPages = main.SiteGetFooterMenu(lang);
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllSocialMedia = main.SiteGetAllSocialMedia();
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            return View(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllPages = main.SiteGetAllPages(lang);
            main.AllSliders = main.SiteGetAllSlider(lang);
            main.MainPageItem = main.SiteGetMainPageItem(lang);
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            return View(main);
        }

        public ActionResult Pages(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Pages = main.SiteGetPage(friendlyUrl);
            return View(main);
        }

        public ActionResult CustomPages(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.Pages = main.SiteGetPage(friendlyUrl);
            return View(main);
        }

        public ActionResult News(string friendlyUrl, string subFriendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllNews = main.SiteGetAllNews(lang);
            main.News = main.SiteGetNew(subFriendlyUrl);
            return View(main);
        }

        public ActionResult Faq(string friendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.Pages = main.SiteGetPage(friendlyUrl);
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllFaqCategories = main.SiteGetAllFaqCategories(lang);
            main.AllFaq = main.SiteGetAllFaq(lang);
            return View(main);
        }

        public ActionResult Policies(string friendlyUrl, string subFriendlyUrl)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllPolicies = main.SiteGetAllPolicies(lang);
            main.Policies = main.SiteGetPolicies(subFriendlyUrl);
            return View(main);
        }

        public ActionResult _RegistrarCompanies()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            main.AllRegistrarCompanies = main.SiteGetAllRegistrarCompanies();
            return View(main);
        }

        public ActionResult _Newsletter()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllTranslates = main.SiteGetAllTranslates(lang);
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _Newsletter(FormCollection fc)
        {
            AdminMainProgress.NewsletterSave(fc["FirstName"], fc["LastName"], fc["Email"], db);
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Redirect(url + "?r=1");
        }

        public ActionResult _ApplicationForm()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _ApplicationForm(FormCollection fc)
        {
            AdminMainProgress.ApplicationFormSave(fc["NameSurname"], fc["Email"], fc["Company"], fc["City"], fc["Country"], fc["RegistrarName"], fc["Website"], fc["Notes"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Redirect(url + "?r=1");
        }

        public ActionResult _ContactForm()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _ContactForm(FormCollection fc)
        {
            AdminMainProgress.ContactFormSave(fc["Name"], fc["Surname"], fc["Email"], fc["Phone"], fc["Subject"], fc["Message"], db);
            ViewBag.Result = true;
            Uri url = System.Web.HttpContext.Current.Request.UrlReferrer;
            return Redirect(url + "?r=1");
        }

        public ActionResult WhoisQuery()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllRegistrarCompanies = main.SiteGetAllRegistrarCompanies();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult WhoisQuery(FormCollection fc)
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            ViewBag.Result = Query(fc["domain"], fc["type"]);
            ViewBag.Domain = fc["domain"];
            ViewBag.Extension = fc["type"];
            return View(main);
        }

        public string Query(string domain, string type)
        {
            string result = DotIstanbulSite.Whois.Lookup(domain, type);
            return result;
        }
    }
}